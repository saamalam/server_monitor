<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <!-- Styles -->
</head>
<body>
<div id="app">
    <div class="container">
        <example-component></example-component>
    </div>
</div>

</body>
<script src="{{mix('js/app.js')}}"></script>

</html>
