@extends('master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Hostname</th>
                                <th>Load Average</th>
                                <th>Total Disk</th>
                                <th>Used Disk</th>
                                <th>Http Status</th>
                                <th>Mysql Status</th>
                                <th>Free Memory</th>
                                <th>#</th>
                            </tr>
                            @foreach($entities as $entity)
                                <tr>
                                    <td>{{$entity->name}}</td>
                                    <td>{{$entity->latestValue['loadaverage']}}</td>
                                    <td>{{$entity->latestValue['disktotal']}}</td>
                                    <td>{{$entity->latestValue['diskused']}}</td>
                                    <td>
                                        @if($entity->latestValue['http'])
                                            <button type="button" class="btn btn-xs btn-success">Open</button>
                                        @else
                                            <button type="button" class="btn btn-xs btn-danger">Closed</button>
                                        @endif
                                    </td>
                                    <td>
                                        @if($entity->latestValue['mysql'])
                                            <button type="button" class="btn btn-xs btn-success">Open</button>
                                        @else
                                            <button type="button" class="btn btn-xs btn-danger">Closed</button>
                                        @endif
                                    </td>
                                    <td>{{$entity->latestValue['memtotal']}}</td>
                                    <td><a href="{{route('values.show', $entity->name)}}">View Detail Graph</a></td>
                                </tr>
                            @endforeach
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
    </div>
    <!-- /.row -->

@endsection