@extends('master')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{ucfirst($entity->name)}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="container">
            <h4>Average Load Status: {{ ucfirst($entity->name) }}</h4>
            {!! $monitorChart->container() !!}

        </div>

        <div class="container">
            <h4>Disk Usage Status: {{ ucfirst($entity->name) }}</h4>
            {!! $highChart->container() !!}
        </div>

        <div class="container">
            <h4>MYSQL Connection Status: {{ ucfirst($entity->name) }}</h4>
            {!! $mysqlChart->container() !!}
        </div>

        <div class="container">
            <h4>HTTP Connection Status Connection Status: {{ ucfirst($entity->name) }}</h4>
            {!! $httpChart->container() !!}
        </div>

        <div class="container">
            <h4>HTTP Connection Status Connection Status: {{ ucfirst($entity->name) }}</h4>
            {!! $testData->container() !!}
        </div>

    </div>
@endsection

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
{!! $monitorChart->script() !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>
{!! $highChart->script() !!}
{!! $mysqlChart->script() !!}
{!! $httpChart->script() !!}
{!! $testData->script() !!}