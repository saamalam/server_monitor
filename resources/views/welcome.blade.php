<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{mix('css/app.css')}}">
    <!-- Styles -->
</head>
<body>
<div id="app">
    <div class="container">
        <p><h4>Average Load Status: {{ ucfirst($entity->name) }}</h4></p>
        {!! $monitorChart->container() !!}

    </div>

    <div class="container">
        <p><h4>Disk Usage Status: {{ ucfirst($entity->name) }}</h4></p>
        {!! $highChart->container() !!}
    </div>

    <div class="container">
        <p><h4>HTTP/MYSQL Connection Status: {{ ucfirst($entity->name) }}</h4></p>
        {!! $mysqlHttpChart->container() !!}
    </div>

</div>

</body>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
{!! $monitorChart->script() !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>
{!! $highChart->script() !!}
{!! $mysqlHttpChart->script() !!}
</html>
