<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>App Monitor - Javra Software</title>

    <link rel="stylesheet" href="{{asset('css/admin.css')}}">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
</head>
<body>

    <div id="wrapper">
        <!-- Navigation -->
            @include('partials.nav')
        <!-- Navigation -->
        <div id="page-wrapper">
            @yield('content')
        </div>
    </div>
<script src="{{asset('js/app.js')}}"></script>

</body>
</html>