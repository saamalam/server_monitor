<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('values', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('entity_id');
            $table->string('hostname')->nullable();
            $table->string('ipaddress')->nullable();
            $table->string('systemuptime')->nullable();
            $table->string('memtotal')->nullable();
            $table->string('memfree')->nullable();
            $table->string('loadaverage')->nullable();
            $table->string('disktotal')->nullable();
            $table->string('diskused')->nullable();
            $table->string('http')->nullable();
            $table->string('mysql')->nullable();
            $table->string('http_est')->nullable();
            $table->string('eth0_tx')->nullable();
            $table->string('eth0_rx')->nullable();
            $table->string('eth1_tx')->nullable();
            $table->string('fathom_port')->nullable();
            $table->string('progressDB_port')->nullable();
            $table->string('appSRV_port')->nullable();
            $table->string('webSPD_port')->nullable();
            $table->string('adminSRV_port')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('values');
    }


}
