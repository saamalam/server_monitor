<?php

use Illuminate\Database\Seeder;

class EntitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Entity::create([
           'name'=>'shaper',
           'url'=>'shaper.javra.com'
        ]);

        \App\Entity::create([
            'name'=>'edelman',
            'url'=>'uatedelman.javra.nl'
        ]);

        \App\Entity::create([
            'name'=>'hkpp',
            'url'=>'uatpolypack.javra.com'
        ]);

        \App\Entity::create([
            'name'=>'gip',
            'url'=>'gipvm6'
        ]);
    }
}
