<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        User::create([
           'name' => 'Javra',
           'email' => 'admin@javra.com',
           'password' => bcrypt('javra123')
        ]);
    }
}
