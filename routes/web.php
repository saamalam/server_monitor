<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', 'EntitiesController@index')->name('dashboard');
Route::resource('entities', 'EntitiesController');
Route::get('values/store', 'ValuesController@store')->name('values.store');
Route::get('values/show/{entity}', 'ValuesController@show')->name('values.show');

Route::get('logout', function() {
   Auth::logout();
   return redirect()->route('login');
});

Route::get('example','ExampleController@test');



