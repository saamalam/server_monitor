<?php

namespace App\Http\Controllers;

use App\Http\Helpers\CsvParser;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\File;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param CsvParser $csvParser
     *
     * @return array
     */
    public function recursiveDirectoryIterator(CsvParser $csvParser): array
    {
        $directoryIterator = new RecursiveDirectoryIterator(storage_path());

        static $parsedData = [];
        foreach (new RecursiveIteratorIterator($directoryIterator) as $filename => $file) {
            if ('csv' !== $file->getExtension()) {
                continue;
            }

            //collects the data and wraps it to the collection instance
            $parsedData[] = collect($filename)->map(function ($file) use ($csvParser) {
                return $csvParser->parseFile($file);
            });

            //File::delete($file);
        }

        return $parsedData;
    }
}
