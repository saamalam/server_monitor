<?php

namespace App\Http\Controllers;

use App\Charts\HighChart;
use App\Charts\MonitorChart;
use App\Entity;
use App\Http\Helpers\CsvParser;
use Illuminate\Support\Facades\Log;

class ValuesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(CsvParser $csvParser)
    {
        //Iterate through a given directory
        $parsedData = $this->recursiveDirectoryIterator($csvParser);
        //maps the $parsedData which is an array, of collection instance
        $savedData = array_map(function ($initialMap) {
            return $initialMap->map(function ($mappedData) {
                //transforming the given data to collection,
                return collect($mappedData)->map(function ($csvData) {
                    Log::info('Retrieved the data',$csvData);
                    $entity = Entity::where('url', $csvData['hostname'])->first();
                    $entity->addValues($csvData);
                    Log::info('Saved the exported Data');
                });
            });
        }, $parsedData);

        if (!$savedData) {
            Log::warning('Could Not Save Data');
        }
    }

    public function show(Entity $entity, MonitorChart $monitorChart)
    {
        $loadAverage = $entity->values()->latest()->pluck('loadaverage')->take(10)->toArray();

        $entryTime = $entity->values()->latest()->pluck('created_at')->take(10)->toArray();

        $monitorChart->labels($entryTime);

        $monitorChart->dataset($entity->name, 'line', $loadAverage)->options([
            'backgroundColor' => '#3490dc',
            'width' => '200',
        ]);

        $highChart = new HighChart();

        $diskUsed = $entity->values()->latest()->pluck('diskused')->take(1)->average();
        $diskTotal = $entity->values()->latest()->pluck('disktotal')->take(1)->average();

        $highChart->labels($entryTime);


        $highChart->dataset($entity->name, 'pie', [($diskUsed/$diskTotal)*100, 100-($diskUsed/$diskTotal)*100])->options([
            'backgroundColor' => 'red',
            'color' => 'blue',
        ]);

        $data = $entity->values()->latest()->select('http', 'mysql')->take(10)->get();

        $httpStatus = collect($data)
            ->pluck('http')->map(function ($http) {
                return (int) trim($http);
            })->toArray();

        $mysqlStatus = collect($data)
            ->pluck('mysql')->map(function ($mysql) {
                return (int) trim($mysql);
            })->toArray();

        $mysqlChart = new HighChart();
        $httpChart = new HighChart();

        $mysqlChart->labels($entryTime);
        $httpChart->labels($entryTime);

        $mysqlChart->dataset('Mysql', 'line', $mysqlStatus);
        $httpChart->dataset('HTTP', 'line', $httpStatus);


        $testData = new HighChart();
        $testData->labels($entryTime);
        $testData->dataset('test', 'spline', $mysqlStatus);



        return view('entities.show', [
          'monitorChart'=>$monitorChart,
          'entity'=>$entity,
          'highChart'=>$highChart,
          'mysqlChart'=>$mysqlChart,
          'httpChart'=>$httpChart,
           'testData'=>$testData
        ]);
    }
}
