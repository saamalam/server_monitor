<?php

namespace App\Http\Controllers;

use App\Entity;


class EntitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $entities = Entity::all();
        return view('dashboard.index', compact('entities'));
    }
}
