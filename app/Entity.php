<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Entity extends Model
{

    protected $with = ['latestValue'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('valueCount', function($builder) {
            $builder->withCount('values');
        });

    }

    public function values()
    {
        return $this->hasMany(Value::class);
    }

    public function addValues($value)
    {
       /*$dataSet = $this->values()->where('ipaddress', $value['ipaddress'])->first();
        collect([$dataSet])->each(function ($data) use ($value) {
            return (is_null($data)) ? $this->values()->create($value) :
                $this->values()->find($data->id)->update($value);
        });*/

        $this->values()->create($value);

    }

    public function getRouteKeyName()
    {
        return 'name';
    }

    public function latestValue()
    {
        return $this->hasOne(Value::class)->latest();
    }
}
