<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Value extends Model
{
    protected $guarded = [];

    public function entity()
    {
        return $this->belongsTo(Entity::class);
    }

    public function sethttpAttribute($value)
    {
        return $this->attributes['http'] = $value === "open" ? 1 : 0;
    }

    public function setmysqlAttribute($value)
    {
        return $this->attributes['mysql'] = $value === "open" ? 1 : 0;
    }

    public function setdiskusedAttribute($value)
    {
        return (int) $this->attributes['diskused'] = trim($value, "G");
    }

    public function setdisktotalAttribute($value)
    {
        return (int) $this->attributes['disktotal'] = trim($value, 'G');
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->diffForHumans();
    }

    public function getmemtotalAttribute($value)
    {
        return $value/(1024*1024);
    }


}
