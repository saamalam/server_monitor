<?php

namespace App\Console\Commands;

use App\Http\Controllers\ValuesController;
use App\Http\Helpers\CsvParser;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ParseCsvFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:csv';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parses the csv file';
    /**
     * @var ValuesController
     */
    private $valuesController;

    /**
     * ParseCsvFile constructor.
     *
     * @param ValuesController $valuesController
     */
    public function __construct(ValuesController $valuesController)
    {
        parent::__construct();
        $this->valuesController = $valuesController;
    }

    /**
     * @param CsvParser $csvParser
     */
    public function handle(CsvParser $csvParser)
    {
        $this->valuesController->store($csvParser);
        $this->info('Csv Parsed Successfully');

    }
}
